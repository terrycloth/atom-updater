# A NodeJS script to check for and install updates to Atom on Fedora

GitHub releases RPM binaries for their code editor
[Atom](https://github.com/atom/atom), but doesn't provide an RPM
*repository* for automatically installing updates. So, until GitHub
provides such a repo, or implements a way for Linux application builds
to update themselves, or until one of the Fedora packagers working on
Atom proves to be a little more reliable about keeping up with upstream,
this is how I do it.

This script parses the Atom releases page on GitHub, compares with the
currently installed version, and downloads and installs the latest
RPM via `dnf`.



## Setup

``` bash
npm install
```



## Command line usage

``` bash
sudo node ./atom-updater.js
```
