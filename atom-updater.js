"use strict";

// Load modules.
const child_process = require( "child_process" );
const fetchPage     = require( "node-fetch" );
const jsdom         = require( "jsdom" );
const { JSDOM }     = jsdom;
const semver        = require( "semver" );

// Get the current Atom version.
let rawVersionString = child_process.execSync( "apm --version --no-color", { encoding: "utf8" });
let currentVersion = rawVersionString.match( /atom\s+(.+)/i )[ 1 ];
console.log( "current :", currentVersion );

// Download and parse the Latest Release page.
let dom;
fetchPage(
    "https://github.com/atom/atom/releases/latest"
).then(
    function ( response ) {
        return response.text();
    }
).then(
    function ( rawHTML ) {
        dom = new JSDOM( rawHTML );
        let versionBlock = dom.window.document.getElementsByClassName( "release-header" )[ 0 ];
        let versionString = versionBlock.getElementsByTagName( "a" )[ 0 ].textContent;
        let latestVersion = semver.clean( versionString );
        console.log( "latest  :", latestVersion );


        if ( semver.gt( latestVersion, currentVersion )) {
            console.log( "Update available!" );
            child_process.execSync(
                `dnf install -y https://github.com/atom/atom/releases/download/v${latestVersion}/atom.x86_64.rpm`,
                {
                    encoding : "utf8",
                    stdio    : "inherit"
                }
            );
        }
        else {
            console.log( "Latest stable release already installed." );
        }
    }
).catch(
    function ( error ) {
        console.error( error );
    }
);
